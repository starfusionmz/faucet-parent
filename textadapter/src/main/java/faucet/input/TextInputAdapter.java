package faucet.input;

import java.util.Map;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;

import faucet.input.mapreduce.MapReduceInputAdapter;
import faucet.model.Record;
import faucet.model.SimpleRecord;
import faucet.model.StringColumn;

public class TextInputAdapter implements MapReduceInputAdapter<LongWritable, Text> {

	public void configure(Map<String, String> config) {
	}

	public Record adapt(Object inputElement) {
		StringColumn textColumn = new StringColumn("LineInFile", inputElement.toString());
		SimpleRecord record = new SimpleRecord();
		record.add(textColumn);
		return record;
	}

	public InputFormat<LongWritable, Text> getInputFormat() {
		return new TextInputFormat();
	}

	public Class<Text> getValueClass() {
		return Text.class;
	}

}
