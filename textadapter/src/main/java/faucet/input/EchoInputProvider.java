package faucet.input;

import faucet.ModuleDescriptor;

/**
 * An {@link InputProvider} that returns an {@link Input} that matches the name
 * in the {@link InputQuery}
 * 
 * @author Mark Lester
 */
public class EchoInputProvider implements InputProvider {
	public EchoInputProvider() {
	}

	public Input locate(InputQuery inputQuery) {
		ModuleDescriptor moduleDescriptor = new ModuleDescriptor("faucet.modules.textadapter", "",
				"faucet.input.TextInputAdapter");
		SimpleInput input = new SimpleInput(inputQuery.getInputName(), inputQuery.getInputName(), inputQuery,
				moduleDescriptor);
		return input;
	}

}
