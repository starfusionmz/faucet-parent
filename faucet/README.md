#Faucet
Faucet is data abstraction library for writing analytics. It separates the roles of ETL and algorithm development.
##Goals
* Be as modular as possible so that flow changes are mostly configuration.
* Common language for asking and using data
##Design
###Input
* Source: where data came from
* Input: 
* InputQuery: container for information on how to retrieve an input
* AdapterProvider: used to locate a provider given a module descripter
* InputAdapter: Takes data located from an input and transform it to the common data model

##Model
Record
Column

##Output

SelectFilter
/phonebook/address[0]/street as firstStreet
//streets as streets