package faucet.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SimpleRecord implements Record{
	List<Column> columns = new ArrayList<>();
	
	public void add(Column column){
		columns.add(column);
	}
	
	@Override
	public Iterator<Column> iterator() {
		return columns.iterator();
	}

}
