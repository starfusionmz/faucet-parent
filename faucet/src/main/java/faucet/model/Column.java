package faucet.model;

public interface Column {
	String getName();
	void setName(String name);
	
	Type getType();
	
	<E> E getValueAs(Class<E> valueType);
	
	Visibility getVisibility();
	void setVisibility(Visibility visibility);
}
