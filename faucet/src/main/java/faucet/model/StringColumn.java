package faucet.model;

public class StringColumn implements Column{
	String name = "";
	Type type;
	Visibility visibility;
	private String value; 
	
	public StringColumn(String name,String value){
		this.value=value;
		this.name=name;
	}
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Type getType() {
		return type;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <E> E getValueAs(Class<E> valueType) {
		if(valueType.isAssignableFrom(String.class)){
			return (E) value;
		}
		return null;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public Visibility getVisibility() {
		return visibility;
	}

	@Override
	public void setVisibility(Visibility visibility) {
		this.visibility = visibility;
	}

}
