package faucet.filters;

import faucet.model.Record;

public interface RecordFilter {
	void filter(Record record);
}
