package faucet.filters;

import faucet.input.Input;

public interface InputFilter {
	void filter(Input input);
}
