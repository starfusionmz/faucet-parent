package faucet;

import org.jboss.modules.LocalModuleLoader;
import org.jboss.modules.Module;
import org.jboss.modules.ModuleLoadException;

import faucet.input.InputProvider;

public class ProviderFactory {
	public static InputProvider create(ModuleDescriptor inputProviderModule) {
		LocalModuleLoader loader = new LocalModuleLoader();
		try {
			Module module = loader.loadModule(inputProviderModule.toModuleID());
			// TODO make this use service instead of class
			Class<?> clazz = module.getClassLoader().loadClass(inputProviderModule.getMainClassName());
			Object provider = clazz.newInstance();
			return (InputProvider) provider;
		} catch (ModuleLoadException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new IllegalStateException("Could not load provider",e);
		}
	}
}
