package faucet.input;
/**
 * Used to provide Inputs
 * @author Mark Lester
 *
 */
public interface InputProvider {
	/**
	 * Given an input query retrieve an Input
	 * @param input
	 * @return
	 * @throws InputNotFoundException
	 */
	Input locate(InputQuery input) throws InputNotFoundException;
}
