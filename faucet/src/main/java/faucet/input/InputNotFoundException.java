package faucet.input;
/**
 * Used to indicate when an input could not be found
 * @author Mark Lester
 *
 */
public class InputNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;
	private String inputName;
	
	public InputNotFoundException(String input) {
		super("Input" +input+" could not be found");
		this.inputName = input;
	}
	
	public InputNotFoundException(String input,Throwable throwable) {
		super("Input" +input+" could not be found",throwable);
		this.inputName = input;
	}
	
	public String getInputName(){
		return inputName;
	}
}
