package faucet.input.mapreduce;

import org.apache.hadoop.mapreduce.InputFormat;

import faucet.input.InputAdapter;
/**
 * Interface that defines what should be in an InputAdapter that adapts to Map Reduce
 * @author Mark Lester
 *
 * @param <K> Key from the MapReduce InputFormat
 * @param <V> Value From the InputFormat
 */
public interface MapReduceInputAdapter<K,V> extends InputAdapter{
	InputFormat<K, V> getInputFormat();
	Class<V> getValueClass();
}
