package faucet.input.mapreduce;

import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapreduce.InputSplit;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import faucet.ModuleDescriptor;

public class FaucetInputSplit extends InputSplit implements Writable{
	SplitBody body;

	public FaucetInputSplit(InputSplit innerSplit,ModuleDescriptor adapterModuleDescriptor) {
		body = new SplitBody(innerSplit, adapterModuleDescriptor);
	}

	@Override
	public long getLength() throws IOException, InterruptedException {
		return getInnerSplit().getLength();
	}

	@Override
	public String[] getLocations() throws IOException, InterruptedException {
		return getInnerSplit().getLocations();
	}
	
	//writable is a pain in the butt so encapsuate state into intermediate object to making serializing easier
	@Override
	public void write(DataOutput out) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		Output output = new Output(baos);
		Kryo kryo = new Kryo();
		kryo.register(SplitBody.class);
		kryo.writeObject(output, body);
		output.close();
		out.writeInt(baos.size());
		out.write(baos.toByteArray());
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		int size = in.readInt();
		byte[] bodyBytes = new byte[size];
		in.readFully(bodyBytes);
		Input input = new Input(bodyBytes);
		Kryo kryo = new Kryo();
		body = kryo.readObject(input, SplitBody.class);
	}
	
	@Override
	public String toString(){
		return getClass().getName()+":"+getInnerSplit().toString();
	}

	public InputSplit getInnerSplit() {
		return body.getInnerSplit();
	}
	
	public ModuleDescriptor getAdapterModule() {
		return body.getAdapterModule();
	}
}
class SplitBody{
	SplitBody(){}
	public SplitBody(InputSplit innerSplit, ModuleDescriptor adapterModule) {
		this.innerSplit = innerSplit;
		this.adapterModule = adapterModule;
	}
	private InputSplit innerSplit;
	private ModuleDescriptor adapterModule;
	public InputSplit getInnerSplit() {
		return innerSplit;
	}
	public void setInnerSplit(InputSplit innerSplit) {
		this.innerSplit = innerSplit;
	}
	public ModuleDescriptor getAdapterModule() {
		return adapterModule;
	}
	public void setAdapterModule(ModuleDescriptor adapterModule) {
		this.adapterModule = adapterModule;
	}
}