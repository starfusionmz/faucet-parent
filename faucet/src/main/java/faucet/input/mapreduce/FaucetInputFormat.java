package faucet.input.mapreduce;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import faucet.AdapterFactory;
import faucet.FaucetConfiguration;
import faucet.ProviderFactory;
import faucet.SerializationException;
import faucet.input.Input;
import faucet.input.InputNotFoundException;
import faucet.input.InputProvider;
import faucet.input.InputQuery;


@SuppressWarnings("rawtypes")
public class FaucetInputFormat extends InputFormat{
	public static final String FAUCET_CONFIGURATION_KEY = "faucet.configuration";
	
	@Override
	public List<InputSplit> getSplits(JobContext context) throws IOException, InterruptedException {
		try {
			FaucetConfiguration faucentConf = getFaucetConfiguration(context.getConfiguration());
			InputProvider inputProvider = ProviderFactory.create(faucentConf.getInputProviderModule());
			List<InputSplit> splits = new ArrayList<>();
			
			for(InputQuery iquery:faucentConf.getInputQueries()){
				Input input = inputProvider.locate(iquery);
				MapReduceInputAdapter<?,?> adapter = AdapterFactory.create(input.getAdapterModule(), MapReduceInputAdapter.class);
				List<InputSplit> innerSplits = adapter.getInputFormat().getSplits(context);
				for(InputSplit inputsplit:innerSplits){
					FaucetInputSplit faucetSplit = new FaucetInputSplit(inputsplit, input.getAdapterModule());					
					splits.add(faucetSplit);
				}
			}
			return splits;
		} catch (SerializationException e) {
			throw new IOException("Could not deserialize Faucet Configuration",e);
		} catch (InputNotFoundException e) {
			throw new IOException("Could not find input "+e.getInputName(),e);
		}
	}

	@Override
	public RecordReader<?, ?> createRecordReader(InputSplit split, TaskAttemptContext context)
			throws IOException, InterruptedException {
		return new FaucetRecordReader();
	}
	
	public static void setFaucetConfiguration(Job job,FaucetConfiguration uberConfiguration) throws SerializationException{
		String serializedConf = FaucetConfiguration.serialize(uberConfiguration);
		job.getConfiguration().set(FAUCET_CONFIGURATION_KEY, serializedConf);
	}
	
	public static FaucetConfiguration getFaucetConfiguration(Configuration job) throws SerializationException{
		String serializedConfig = job.get(FAUCET_CONFIGURATION_KEY);
		return FaucetConfiguration.deserialize(serializedConfig);
	}
}
