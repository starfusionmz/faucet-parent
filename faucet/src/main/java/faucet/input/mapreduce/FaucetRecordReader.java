package faucet.input.mapreduce;

import java.io.IOException;

import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;

import faucet.AdapterFactory;
import faucet.input.Source;
import faucet.model.Record;

public class FaucetRecordReader extends RecordReader<Source, Record> {
	private RecordReader<?, ?> innerRecordReader;
	MapReduceInputAdapter<?, ?> inputAdapter;
	private Source source;
	
	@Override
	public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
		FaucetInputSplit fsplit = (FaucetInputSplit)split;
		inputAdapter = AdapterFactory.create(fsplit.getAdapterModule(),MapReduceInputAdapter.class);
		innerRecordReader = inputAdapter.getInputFormat().createRecordReader(fsplit.getInnerSplit(), context);
	}

	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		return innerRecordReader.nextKeyValue();
	}

	@Override
	public Source getCurrentKey() throws IOException, InterruptedException {
		return source;
	}

	@Override
	public Record getCurrentValue() throws IOException, InterruptedException {
		Object val = innerRecordReader.getCurrentValue();
		return inputAdapter.adapt(val);
	}

	@Override
	public float getProgress() throws IOException, InterruptedException {
		return innerRecordReader.getProgress();
	}

	@Override
	public void close() throws IOException {
		innerRecordReader.close();
	}

}
