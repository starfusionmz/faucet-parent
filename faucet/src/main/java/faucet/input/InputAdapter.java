package faucet.input;

import java.util.Map;

import faucet.model.Record;

public interface InputAdapter {
	void configure(Map<String,String> config);
	Record adapt(Object inputElement);
}
