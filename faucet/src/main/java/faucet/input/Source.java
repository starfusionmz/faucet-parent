package faucet.input;

/**
 * This represent where data came from
 * @author Mark Lester
 *
 */
public interface Source {
	/**
	 * the name of the
	 * @return
	 */
	String getName();
	/**
	 * get the name of underlying source
	 * @return
	 */
	String getInputName();
}
