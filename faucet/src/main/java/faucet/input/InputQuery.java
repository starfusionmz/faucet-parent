package faucet.input;

import java.util.Set;
import java.util.TreeSet;

import faucet.filters.InputFilter;
import faucet.filters.RecordFilter;

/**
 * Used to pass query predicates to an {@link InputProvider} to get an
 * {@link Input}
 * 
 * @author Mark Lester
 *
 */
public class InputQuery {
	private String inputName;
	private Set<InputFilter> inputFilters = new TreeSet<>();
	private Set<RecordFilter> recordFilters = new TreeSet<>();

	public InputQuery(String inputName) {
		this.inputName = inputName;
	}

	public String getInputName() {
		return inputName;
	}

	public void setInputName(String inputName) {
		this.inputName = inputName;
	}

	public Set<InputFilter> getInputFilters() {
		return inputFilters;
	}

	public void addInputFilter(InputFilter inputFilter) {
		this.inputFilters.add(inputFilter);
	}

	public Set<RecordFilter> getRecordFilters() {
		return recordFilters;
	}

	public void addRecordFilter(RecordFilter recordFilter) {
		this.recordFilters.add(recordFilter);
	}
}
