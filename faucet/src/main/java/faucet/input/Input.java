package faucet.input;

import faucet.ModuleDescriptor;

/**
 * An Input is a located place
 * where data can come in from. An Input can have many adapters depending on
 * what tech is used to read the data
 * 
 * @author Mark Lester
 *
 */
public interface Input extends Source{
	InputQuery getInputQuery();
	ModuleDescriptor getAdapterModule();
}
