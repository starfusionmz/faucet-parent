package faucet.input;

import faucet.ModuleDescriptor;

public class SimpleInput implements Input{
	String name;
	String inputName;
	InputQuery inputQuery;
	ModuleDescriptor adapterModule;
	
	public SimpleInput(String name,String inputName,InputQuery inputQuery,ModuleDescriptor adapterModule) {
		this.name = name;
		this.inputName = inputName;
		this.inputQuery = inputQuery;
		this.adapterModule = adapterModule;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getInputName() {
		return inputName;
	}

	@Override
	public InputQuery getInputQuery() {
		return inputQuery;
	}

	@Override
	public ModuleDescriptor getAdapterModule() {
		return adapterModule;
	}

}
