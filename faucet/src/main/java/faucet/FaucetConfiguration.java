package faucet;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;

import faucet.input.InputQuery;

public class FaucetConfiguration {
	private Set<InputQuery> inputQueries;
	private ModuleDescriptor inputProviderModule;
	
	public static String serialize(FaucetConfiguration config) throws SerializationException{
		ObjectMapper mapper = new ObjectMapper();
		StringWriter writer = new StringWriter();
		try {
			mapper.writeValue(writer, config);
		} catch (IOException e) {
			throw new SerializationException("Could not serialize uber config", e);
		}
		return writer.toString();
	}

	public static FaucetConfiguration deserialize(String serializedConfig) throws SerializationException {
		ObjectMapper mapper = new ObjectMapper();
		FaucetConfiguration config;
		try {
			config = mapper.readValue(serializedConfig, FaucetConfiguration.class);
		} catch (IOException e) {
			throw new SerializationException("Could not deserialize uber config",e);
		}
		return config;
	}
	/**
	 * get the module used to find inputs
	 * @return
	 */
	public ModuleDescriptor getInputProviderModule() {
		return inputProviderModule;
	}
	/**
	 * set the module used to find inputs
	 * @return
	 */
	public void setInputProviderModule(ModuleDescriptor inputProviderModule) {
		this.inputProviderModule = inputProviderModule;
	}

	public Set<InputQuery> getInputQueries() {
		return inputQueries;
	}

	public void setInputQueries(Set<InputQuery> inputQueries) {
		this.inputQueries = inputQueries;
	}
	
}
