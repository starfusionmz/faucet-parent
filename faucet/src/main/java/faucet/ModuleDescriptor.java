package faucet;

import org.jboss.modules.ModuleIdentifier;

public class ModuleDescriptor {
	public ModuleDescriptor(){
		
	}
	
	public ModuleDescriptor(String name, String version, String mainClassName) {
		this.name = name;
		this.version = version;
		this.mainClassName = mainClassName;
	}
	
	private String name;
	private String version;
	private String mainClassName;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public ModuleIdentifier toModuleID(){
		return ModuleIdentifier.create(name,version);
	}
	public String getMainClassName() {
		return mainClassName;
	}
	public void setMainClassName(String mainClassName) {
		this.mainClassName = mainClassName;
	}
}
