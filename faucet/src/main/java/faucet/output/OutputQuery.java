package faucet.output;

public interface OutputQuery {
	String getName();
	void setName(String name);
	Intent getIntent();
	void setIntent(Intent intent);
}
