package faucet;

import org.jboss.modules.LocalModuleLoader;
import org.jboss.modules.Module;
import org.jboss.modules.ModuleLoadException;

public class AdapterFactory {
	@SuppressWarnings("unchecked")
	public static <E> E create(ModuleDescriptor adapterModule,Class<E> adapterClass){
		LocalModuleLoader loader = new LocalModuleLoader();
		try {
			Module module = loader.loadModule(adapterModule.toModuleID());
			// TODO make this use service instead of class
			Class<?> clazz = module.getClassLoader().loadClass(adapterModule.getMainClassName());
			Object provider = clazz.newInstance();
			return (E) provider;
		} catch (ModuleLoadException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new IllegalStateException("Could not load adapter",e);
		}
	}
}
