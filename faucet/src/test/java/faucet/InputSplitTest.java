package faucet;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import faucet.input.mapreduce.FaucetInputSplit;

public class InputSplitTest {

	@Test
	public void testSerializeDeserialize() throws IOException {
		FileSplit fsplit = new FileSplit();
		ModuleDescriptor md = new ModuleDescriptor("test","1.0.0","class");
		FaucetInputSplit faucetSplit = new FaucetInputSplit(fsplit,md);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream das = new DataOutputStream(baos);
		faucetSplit.write(das);
		das.close();
		ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());
		DataInputStream dis = new DataInputStream(bis);
		faucetSplit.readFields(dis);
		ObjectMapper mapper = new ObjectMapper();
		String expected = mapper.writeValueAsString(fsplit);
		String actual = mapper.writeValueAsString(faucetSplit.getInnerSplit());
		Assert.assertEquals(expected, actual);
	}

}
