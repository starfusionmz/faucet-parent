package faucet;
import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import faucet.input.EchoInputProvider;
import faucet.input.Input;
import faucet.input.InputNotFoundException;
import faucet.input.InputProvider;
import faucet.input.InputQuery;

public class InputProviderTest {
	
	@Test
	public void testCreateInputProvider() throws InputNotFoundException{
		System.out.println(new File(".").getAbsolutePath());
		System.setProperty("module.path", "target/jboss-modules");
		ModuleDescriptor moduleDescriptor = new ModuleDescriptor("faucet.modules.textadapter","main" , EchoInputProvider.class.getName());
		InputProvider inputProvider = ProviderFactory.create(moduleDescriptor);
		String inputName=  "testInput";
		InputQuery inputQuery = new InputQuery(inputName);
		Input input = inputProvider.locate(inputQuery);
		Assert.assertEquals(inputName, input.getInputName());
	}
}
